# ServerFacil Centos para magento

Desenvolvido por: <a href="http://www.oitoo.com.br"> Oitoo </a>
Contato: contatooitoo@gmail.com

Este script faz uma instalação simples, porém eficiente de um servidor para lojas magento. 
Ele instala ou configura os seguintes itens:
<ul>
<li>Nginx 1.8.0</li>
<li>Php 5.3.3</li>
<li>Php-fpm</li>
<li>Php-apc</li>
<li>Percona mysql 0.0.2-3</li>
<li>Phpmyadmin 4.0.10.11</li>
<li>E-mail pelo gmail</li>
<li>Configurações de segurança para nginx(exploit, sql injection, spam, ...)</li>
<li>Firewall</li>
</ul>

Com ele é possível efetuar a instalação de vários domínios com lojas diferntes em um mesmo servidor, separado por contas, isolando cada aplicação.

<h2>Instalação</h2>

Acesse seu vps por ssh e digite os seguintes comandos:

<pre>
yum install git
git clone https://github.com/alexbraga/ServerFacil.git
chmod -Rf 777 ServerFacil/
cd ServerFacil
</pre>

Depois basta rodas o script, executando o seguinte comando:

<pre>
./install.sh
</pre>

O processo é auto-explicativo e bastante simples. No final você terá um servidor pronto para rodar sua loja magento.

<h3>Requisitos mínimos</h3>
<ul>
<li>centos (6.x ou 7.x) ou amazon linux </li>
<li>Sistema 64bits</li>
<li>Instalação com mysql 1024MB / instalação sem mysql 512MB</li>
<li>5gb de hd</li>
</ul>

<h3>Requisistos recomendados</h3>
<ul>
<li>centos (6.x ou 7.x) ou amazon linux </li>
<li>Sistema 64bits</li>
<li>Instalação com mysql 2048MB / instalação sem mysql 1024MB</li>
<li>20gb de hd</li>
</ul>

<h2>Informações úteis</h2>
<h3>Quantos acessos minha loja aguentará se o servidor for instalado com este script?</h3>
Essa é uma questão um pouco complicada de ser respondida, porém podemos fazer uma estimativa de quantos visitantes simultânios a loja aguentará com um calculo simples:

<pre>
Visitantes simultâneos = (Memória do servidor em MB -700MB) / 70
</pre>

Ou seja, se seu servidor possui 2gb(2048MB) de memória, vamos subtrair 700MB e dividir por 70;

<pre>
Visitantes simultâneos = (2048-700)/70 = 19
</pre>

Neste caso, sua loja suportará 19 clientes conectados ao mesmo tempo.

Neste exemplo estamos considerando que cada processo do php vai utilizar 70MB(que é o padrão), não estamos levando em consideração o apc, que diminui o consumo de memória de cada processo, além de não levar, também, em consideração que alguns clientes podem estar e ações criticas, como a finalização de um pedido, que pode consumir mais de 200mb.

Portanto, o ideal é instalar um sistema de monitoramento no servidor para termos uma base melhor de quantos acessos ele pode suportar. Eu recomendo a utilização do Newrelic(http://newrelic.com/).

Para finalizar, outro fator importante é a empresa de hospedagem, pois de nada vale uma excelente configuração de servidor, se o host não for bom.

<h3>Onde contratar um VPS?</h3>

Essa é outra questão um pouco complicada, porém vou deixar algumas sugestoes pessoais onde já tive excelentes experiências:

<h4>Com datacenters no Brasil</h4>
<ul>
<li>Amazon ec2</li>
<li>Embratel</li>
</ul>

<h4>Com datacenters no Exterior</h4>
<ul>
<li>Linode</li>
<li>DigitalOcean</li>
</ul>

*Estas são sugestões minhas, não quer dizer que são as melhores empresas do mundo. :)

<h3>Como ficará o fluxo de dados do meu servidor após instalar este script?</h3>

O fluxo é bastante simples:
<pre>
Mysql -> Php-fpm -> Nginx -> Cliente
</pre>
Em futuras versões o script dará suporte ao varnish, hhvm entre outros, porém por enquanto ele está sendo mantido para ser simples, rápido e funcional.

<h3>Como enviar minha loja para o servidor depois que ele for instalado?</h3>

Você poderá utilizar o cliente de ftp filezilla. É importante lembrar que você precisa configurar o acesso como "sftp" e colocar os dados de acesso do ssh no usuário e senha. No caso da amazon utilizar a chave de acesso.
<pre>
A pasta padrão apra arquivos é /home/{{USUARIOdoVIRTUALHOSTCRIADO}}/www
</pre>

<h3>Posso instalar um certificado de segurança(ssl)?</h3>

Sim, o script já deixa toda a configuração pronta, porém para executar esta trarefa você precisa ter algum conhecimento em configuração de servidores. Caso você não tenha entre em contato conosco.

<h3>Como acessar o phpmyadmin após a instalação?</h3>

Simples, basta digitar "/phpmyadmin" após o domínio da sua loja. ex: www.sualoja.com.br/phpmyadmin
O usuário e a senha são os mesmos escolhidos no momento da instalação do virtualhost









