#!/bin/bash

#faz verificações de versão e requisitos mínimos
#verifica se sistema tem 64bits
case $(uname -m) in
x86_64)
    echo "Verificação de bits concluida!"
    ;;
*)
    echo "Este sistema possui apenas 2 bits, por isso não vai ser possível proceguir com a instalação. Por favor instale um sistema com 64Bits."
    exit 1
    ;;
esac

yum install which -y
#verifica se distribuição a distribuição possui o comando yum
YUM_CMD=$(which yum)
 if [[ ! -z $YUM_CMD ]]; then
      echo "Distribuição compatível com o script!"
 else
     echo "Sua distribuição parece não ser contpatível com este script. Por favor utilize centos, Amazon linux ou Hedhat"
    exit 1
 fi


#Verfica a versão do sistema
CENTOSV=$(rpm -q --qf "%{VERSION}" $(rpm -q --whatprovides redhat-release))
CENTOSVERSION=${CENTOSV%.*}

case $CENTOSVERSION in
6)
    #instal repositório para o centos 6
    wget http://nginx.org/packages/centos/6/noarch/RPMS/nginx-release-centos-6-0.el6.ngx.noarch.rpm
    rpm -ivh nginx-release-centos-6-0.el6.ngx.noarch.rpm
    
    #instala repositório epel para centos 6
    wget http://download.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm
    rpm -ivh epel-release-6-8.noarch.rpm
    ;;
7)
    #instala repositório para centos 7
    wget http://nginx.org/packages/centos/7/noarch/RPMS/nginx-release-centos-7-0.el7.ngx.noarch.rpm
    rpm -ivh nginx-release-centos-7-0.el7.ngx.noarch.rpm
  
    #instala repositório epel para centos 7
    wget http://dl.fedoraproject.org/pub/epel/7/x86_64/e/epel-release-7-5.noarch.rpm
    rpm -ivh epel-release-7-5.noarch.rpm
    ;;
*)
    echo "Esta versão do centos não é suportada por este script. Você precisa da versão 6.x ou 7.x."
    exit 1
    ;;
esac


while true; do
    read -p "Deseja instalar o banco de dados mysql neste servidor? y/n" yn
    case $yn in
        [Yy]* ) INSTALARMYSQL="YES"; break;;
        [Nn]* ) INSTALARMYSQL="NO"; break;;
        * ) echo "Por favor, digite y ou n.";;
    esac
done

if [ "$INSTALARMYSQL" == "YES" ]; then
    echo "Digite uma senha para o usuário root banco de dados:"
    read SENHA
fi


echo "Desinstalando servidores web existentes"
yum remove httpd -y

echo "Instalando git"
yum install git -y

echo "Instalação do nginx:"
cd /tmp

yum install nginx -y
chkconfig nginx on
service nginx start


if [ "$INSTALARMYSQL" == "YES" ]; then
    echo "Instalação do mysql:"
    yum install -y http://www.percona.com/downloads/percona-release/redhat/0.1-3/percona-release-0.1-3.noarch.rpm
    yum install Percona-Server-server-56 Percona-Server-client-56 -y
    service mysql start 
    chkconfig mysql on
    mysqladmin -u root password $SENHA
fi


echo "Instalação das bibliotecas necessárias para o magento:"
yum install php-fpm -y
yum install php-mcrypt -y
yum install php-dom -y
yum install php-soap -y
yum install php-gd -y
yum install php-pdo -y
yum install php-mysql -y
yum install php-mbstring -y

chkconfig php-fpm on

mkdir /usr/share/phpmyadmin
cd /usr/share/phpmyadmin/
git clone https://github.com/alexbraga/phpmyadmin.git
mv phpmyadmin/* ./

chmod -R 777 *  

mkdir /var/lib/php/session
chmod -R 777 /var/lib/php/session

echo "Instalando o apc"
yum install php-pecl-apc -y

echo "Instalando cron"
yum install cronie -y

#libera acesso para novos usuários
groupadd sftp

echo "Match Group sftp
       X11Forwarding no
       AllowTcpForwarding no
       PermitTTY no
       ForceCommand cvs server" >> /etc/ssh/sshd_config

service sshd restart       

echo "Sistema base instalado com sucesso!"
