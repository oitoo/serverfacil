echo "Este script vai configurar seu servidor para enviar e-maisl a partir de uma conta gmail previamente criada. Essa conta pode ser do google apps. "
echo "Atualmente este script só é compativel com emails do gmail. Caso você pule essa configuração, sua loja não enviará os e-mails transacionais aos seus clientes."

while true; do
    read -p "Deseja vincular este servidor a uma conta do gmail (y/n)?" yn
    case $yn in
        [Yy]* ) 
                echo Digite seu usuario Gmail
                read USERGMAIL
                echo Digite sua senha do Gmail
                read SENHAGMAIL
                
                echo Desinstalando postfix
                service postfix stop && chkconfig postfix off
                
                echo Instalando mailx e ssmtp
                yum install mailx ssmtp -y
                
                echo "root=$USERGMAIL
                mailhub=smtp.gmail.com:587
                hostname=$USERGMAIL
                UseSTARTTLS=Yes
                AuthUser=$USERGMAIL
                AuthPass=$SENHAGMAIL
                FromLineOverride=yes
                TLS_CA_File=/etc/pki/tls/certs/ca-bundle.crt
                " > /etc/ssmtp/ssmtp.conf
                
                echo Digite um e-mail para ser enviada uma mensagem de teste
                read EMAIL
                
                echo Enviando e-mail de teste
                echo "Esta é uma mensagem de teste" | mail -s "Teste do novo servidor" $EMAIL
                
                echo Se não ocorreu nenhum erro, o servidor de e-mails foi instalado com sucesso!
        ;;
        [Nn]* ) exit;;
        * ) echo "Por favor, digite y ou n.";;
    esac
done




